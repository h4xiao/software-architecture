#! /usr/bin/env python

# Written by Sukhbir Singh, Han Xiao and Ashar Ghani.
# Released under GPL.
# University of Waterloo CS 746 Project (Fall 2012).

# Using Python's AST.

"""Python's grammar for reference:
        http://docs.python.org/release/2.5.2/ref/grammar.txt
"""

import os
import ast
import sys
import shutil
import fnmatch

DIR = ''
FILES = []
COUNT = 0
ERROR = 0
OUTFILE = ''


# Write TA information to the file.
def write_ta(line):
    with open(OUTFILE, 'a') as f:
        f.write(line)
        f.write('\n')


# Return a list of the specific type: def, class ...
def get_all_type(type_):
    return_dict = {}

    if type_ == 'def':
        expr_type = ast.FunctionDef
    if type_ == 'class':
        expr_type = ast.ClassDef

    for FILE in FILES:
        with open(FILE) as f:
            source = f.read()
        try:
            expr = ast.parse(source)
        except SyntaxError:
            global ERROR
            ERROR += 1
            continue

        for func in expr.body:
            if isinstance(func, expr_type):
                global COUNT
                return_dict[os.path.basename(FILE).split('.')[0] + '.' + func.name] = {'file': FILE,
                                                                                       'func_count': COUNT,
                                                                                       'class_method': False}
                COUNT += 1

    return return_dict


# Process class information.
def process_classes(functions):
    """
    classdef ::= 
                 "class" classname [inheritance] ":"
                  suite
    """
    for FILE in FILES:
        with open(FILE) as f:
            source = f.read()
        try:
            expr = ast.parse(source)
        except SyntaxError:
            global ERROR
            ERROR += 1
            continue

        for each in expr.body:
            if isinstance(each, ast.ClassDef):
                methods = [method.name for method in each.body if isinstance(method, ast.FunctionDef)]
                for method in methods:
                    global COUNT
                    filename = os.path.basename(FILE).split('.')[0]
                    functions[filename + '.' + each.name + '.' + method] = {'class_method': True,
                                                                            'file': FILE,
                                                                            'func_count': COUNT,
                                                                            'class': filename + '.' + each.name}
                    COUNT += 1


def write_class_info(functions):
    for FILE in FILES:
        with open(FILE) as f:
            source = f.read()
        try:
            expr = ast.parse(source)
        except SyntaxError:
            global ERROR
            ERROR += 1
            continue

        for each in expr.body:
            if isinstance(each, ast.ClassDef):
                methods = [method for method in each.body if isinstance(method, ast.FunctionDef)]
                for method in methods:
                    method.name = os.path.basename(FILE).split('.')[0] + '.' + each.name + '.' + method.name
                    for func in functions:
                        if functions[func]['class_method']:
                            if functions[func]['file'] == FILE:
                                if functions[func]['class'].split('.')[-1] == each.name:
                                    dump = ast.dump(method)
                                    func = func.split('.')[-1]
                                    if ("attr='"+func in dump) or ("id='"+func in dump):
                                        if func != method.name:
                                            write_ta(
                                                    '(CCF F{0} F{1}) ' \
                                                    '{{file="{2}" freq=1}}'.format(
                                                        functions[method.name]['func_count'],
                                                        functions[os.path.basename(FILE).split('.')[0] + '.' + each.name + '.' + func]['func_count'],
                                                        FILE)
                                                    )


# Process function information.
def process_functions(functions):
    """
    funcdef ::= 
                 [decorators] "def" funcname "(" [parameter_list] ")"
                  ":" suite
    """
    for FILE in FILES:
        with open(FILE) as f:
            source = f.read()
        try:
            expr = ast.parse(source)
        except SyntaxError:
            global ERROR
            ERROR += 1
            continue

        for each in expr.body:
            if isinstance(each, ast.FunctionDef):
                each.name = os.path.basename(FILE).split('.')[0] + '.' + each.name
                dump = ast.dump(each)
                for func in functions:
                    if functions[func]['file'] == FILE:
                        if functions[func]['class_method']:
                            continue
                        func = func.split('.')[1]
                        if ("id='"+func in dump) or ("attr='"+func in dump):
                            func = os.path.basename(FILE).split('.')[0] + '.' + func
                            if func != each.name:
                                write_ta(
                                         '(CGF F{0} F{1}) ' \
                                         '{{file="{2}" freq=1}}'.format(functions[each.name]['func_count'],
                                                                        functions[func]['func_count'],
                                                                        FILE)
                                         )


# Default scheme, just write it to TA as it is.
def write_scheme():
    shutil.copyfile('scheme.data', OUTFILE)


def write_fact_tuple(functions, classes):
    write_ta('FACT TUPLE :')
    
    # When we update file into a list of files, then we can change this.
    for i, FILE in enumerate(FILES, 1):
        write_ta('$INSTANCE S{0} S'.format(i))

    for i, class_ in enumerate(classes):
        write_ta('$INSTANCE C{0} C'.format(i))
        class_index = FILES.index(classes[class_]['file']) + 1
        classes[class_]['index'] = i
        write_ta('contain S{0} C{1}'.format(class_index, i)) 

    for key in functions.iterkeys():
        func_index = FILES.index(functions[key]['file']) + 1
        func_name = functions[key]['func_count']
        write_ta('$INSTANCE F{0} {1}F'.format(func_name, 'C' if functions[key]['class_method'] else ''))
        if functions[key]['class_method']:
            class_index = classes[functions[key]['class']]['index']
            write_ta('contain C{0} F{1}'.format(class_index, func_name))
        else:
            write_ta('contain S{0} F{1}'.format(func_index, func_name))


def write_fact_attribute(functions, classes):
    write_ta('FACT ATTRIBUTE :')
    for i, FILE in enumerate(FILES, 1):
        fileName = FILE
        if os.path.isdir(DIR):
            fileName = os.path.relpath(FILE, DIR)
        
        # convert "path\filename.py" to "paht\\filename.py" as LSEdit also considers '\' as an escape symbol     
        doubleEscapesFileName = fileName.replace ("\\", "\\\\")
        
        write_ta('S{0} {{label="{1}" file="{1}"}}'.format(i, doubleEscapesFileName))

    for key in functions.iterkeys():
        write_ta('F{0} {{label="{1}" file="{2}"}}'.format(functions[key]['func_count'], key.split('.')[-1],
                                                          functions[key]['file']))
    for i, class_ in enumerate(classes):
        write_ta('C{0} {{label="{1}" file="{2}"}}'.format(i, class_.split('.')[1],
                                                          classes[class_]['file']))


def get_all_files():
    # Let's get all the py files. Recursively of course.
    for root, dirs, files in os.walk(DIR):
        for each in files:
            if fnmatch.fnmatch(each, '*.py'):
                FILES.append(os.path.join(root, each))


def main():
    if os.path.isfile(DIR):
        if fnmatch.fnmatch(DIR, '*.py'):
            FILES.append(DIR)
        else:
            sys.exit('{0} is not a python file'.format(DIR))
    else:
        get_all_files()
    write_scheme()

    # Most of what follows below is TA specific stuff and how a TA file is
    # formatted. Going into the details here is not possible, but if you are
    # interested, have a look at the TA file to see what is happening.
    functions = get_all_type('def')
    classes = get_all_type('class')

    process_classes(functions)
    write_fact_tuple(functions, classes)
    write_fact_attribute(functions, classes)

    write_class_info(functions)
    process_functions(functions)

    print 'TA file saved to {0}'.format(os.path.abspath(OUTFILE))
    if ERROR:
        print 'There were {0} parsing errors in the current run'.format(ERROR)
    else:
        print 'No errors during parsing'


if __name__ == '__main__':
    if len(sys.argv) < 2:
        sys.exit("Usage: {0} [directory | python file]".format(__file__))

    DIR = sys.argv[1]

    OUTFILE = '{0}.ta'.format(os.path.basename(os.path.abspath(DIR)))
    open(OUTFILE, 'w').close()

    if os.path.isdir(DIR):
        print 'Reading directory {0} ...'.format(os.path.abspath(DIR))
    else:
        print 'Reading file {0} ...'.format(os.path.abspath(DIR))

    main()
