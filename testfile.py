def first_func():
    print 'Hello!'

def second_func(a, b):
    print 'I will call first'
    first_func()

def third_func():
    print 'I will call first and second'
    second_func("asda", "asdsa")
    first_func()
